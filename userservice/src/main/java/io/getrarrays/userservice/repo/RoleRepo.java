package io.getrarrays.userservice.repo;

import io.getrarrays.userservice.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Lordkikes
 * @version 1.0
 * @since 08/02/2022
 */

public interface RoleRepo extends JpaRepository<Role, Long> {

    Role findByName(String name);
}
