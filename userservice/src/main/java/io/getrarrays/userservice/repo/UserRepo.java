package io.getrarrays.userservice.repo;

import io.getrarrays.userservice.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Lordkikes
 * @version 1.0
 * @since 08/02/2022
 */

public interface UserRepo extends JpaRepository<User, Long> {

    User findByUsername(String username);
}
