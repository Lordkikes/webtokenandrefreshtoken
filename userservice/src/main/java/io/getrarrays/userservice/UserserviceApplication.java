package io.getrarrays.userservice;

import io.getrarrays.userservice.domain.Role;
import io.getrarrays.userservice.domain.User;
import io.getrarrays.userservice.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

/**
 * @author Lordkikes
 * @version 1.0
 * @since 08/02/2022
 */

@SpringBootApplication
public class UserserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserserviceApplication.class, args);
    }

    @Bean
    PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    CommandLineRunner run(UserService userService){
        return  args -> {
            userService.saveRole(new Role(null, "ROLE_USER"));
            userService.saveRole(new Role(null, "ROLE_MANAGER"));
            userService.saveRole(new Role(null, "ROLE_ADMIN"));
            userService.saveRole(new Role(null, "ROLE_SUPER_ADMIN"));

            userService.saveUser(new User(null, "Kike Navarro", "kike", "1234", new ArrayList<>()));
            userService.saveUser(new User(null, "Tatiana Alfaro", "tati", "1234", new ArrayList<>()));
            userService.saveUser(new User(null, "Oryanna Navarro", "ory", "1234", new ArrayList<>()));
            userService.saveUser(new User(null, "Camilo Navarro", "cami", "1234", new ArrayList<>()));

            userService.addRoleToUser("tati", "ROLE_USER");
            userService.addRoleToUser("tati", "ROLE_MANAGER");
            userService.addRoleToUser("ory", "ROLE_ADMIN");
            userService.addRoleToUser("cami", "ROLE_USER");
            userService.addRoleToUser("kike", "ROLE_SUPER_ADMIN");
            userService.addRoleToUser("kike", "ROLE_ADMIN");
            userService.addRoleToUser("kike", "ROLE_USER");

        };
    }

}
