package io.getrarrays.userservice.service;

import io.getrarrays.userservice.domain.Role;
import io.getrarrays.userservice.domain.User;

import java.util.List;

/**
 * @author Lordkikes
 * @version 1.0
 * @since 08/02/2022
 */

public interface UserService {

    User saveUser(User user);
    Role saveRole(Role role);
    void addRoleToUser(String username, String roleName);
    User getUser(String username);
    List<User> getUsers();
}
